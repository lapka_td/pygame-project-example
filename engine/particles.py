import random
from enum import Enum

import pygame

from .utils import (
    get_random_point_in_circle,
    get_random_point_in_rect,
    get_random_color,
    get_random_float,
    get_random_vector,
)


class Particle:
    def __init__(self, pos=(0, 0), velocity=(0, 0), acceleration=0, animation=None, color=None, lifetime=0):
        self.pos = list(pos)
        self.velocity = list(velocity)
        self.acceleration = acceleration
        if animation:
            self.animation = animation.copy()
        else:
            self.animation = None
        self.color = color
        self.lifetime = lifetime
        self.enabled = True

    def reset(self):
        self.enabled = True
        if self.animation:
            self.animation.reset()

    def update(self, dt: float) -> bool:
        if not self.enabled:
            return False
        if self.animation:
            if self.animation.done:
                self.enabled = False
                return False
            self.animation.update(dt)
        elif self.lifetime > 0:
            self.lifetime -= dt
            if self.lifetime <= 0:
                self.enabled = False
                return False

        self.pos[0] += self.velocity[0] * dt
        self.pos[1] += self.velocity[1] * dt
        self.velocity[0] += self.acceleration * dt
        self.velocity[1] += self.acceleration * dt
        return True

    def render(self, surface, offset=(0, 0)):
        if not self.enabled:
            return
        if self.animation:
            img = self.animation.img()
            surface.blit(
                img,
                (self.pos[0] + offset[0] - img.get_width() // 2, self.pos[1] + offset[1] - img.get_height() // 2),
            )
        else:
            pygame.draw.circle(surface, self.color, (self.pos[0] + offset[0], self.pos[1] + offset[1]), 1)


class SortType(Enum):
    NO_SORT = 0  # Fast
    SPAWN_BOTTOM = 1  # VERY expensive
    SPAWN_TOP = 2  # Expensive


class ParticleSystem:
    def __init__(
        self,
        pos,  # [x, y]
        emitter,  # Point - (x, y) / Circle - (x, y, radius) / Rect - (x, y, w, h)
        period_sec=0,  # Time between emitting
        periods=0,  # How many times it will emit (0 - endless)
        batch_min=1,  # How much particles will emitted every period_sec
        batch_max=1,
        animation=None,
        color_min=None,  # Affects only if no animation present
        color_max=None,
        lifetime_min=0,  # 0 - particle won't dead
        lifetime_max=1.0,
        angle_min=0,  # Zero is at 3 o'clock
        angle_max=365,
        velocity_min=0,
        velocity_max=0,
        acceleration_min=0,
        acceleration_max=0,
        sort_type=SortType.NO_SORT
    ):
        self.pos = list(pos)
        self.emitter = emitter
        self.period_sec = period_sec
        self.periods = periods
        self.batch_min = batch_min
        self.batch_max = batch_max
        self.animation = animation
        self.color_min = color_min
        self.color_max = color_max
        self.lifetime_min = lifetime_min
        self.lifetime_max = lifetime_max
        self.angle_min = angle_min
        self.angle_max = angle_max
        self.velocity_min = velocity_min
        self.velocity_max = velocity_max
        self.acceleration_min = acceleration_min
        self.acceleration_max = acceleration_max
        self.sort_type = sort_type

        self.particles = []
        self.time_acc = 0
        self.times = 0
        self.enabled = True

    def spawn_particle(self) -> Particle:
        dead = -1
        for i in range(len(self.particles)):
            if not self.particles[i].enabled:
                dead = i
                break
        if dead < 0:
            if self.sort_type == SortType.SPAWN_BOTTOM:
                self.particles.insert(0, Particle(animation=self.animation))
                return self.particles[0]
            else:
                self.particles.append(Particle(animation=self.animation))
                return self.particles[-1]
        else:
            self.particles[dead].reset()
            if self.sort_type == SortType.SPAWN_BOTTOM:
                self.particles.insert(0, self.particles.pop(dead))
                return self.particles[0]
            elif self.sort_type == SortType.SPAWN_TOP:
                self.particles.append(self.particles.pop(dead))
                return self.particles[-1]
            else:
                return self.particles[dead]

    def update(self, dt: float) -> bool:
        # Ended up, nothing to do
        if not self.enabled:
            return False

        # Update and count live particles
        live = 0
        for particle in self.particles:
            live += 1 if particle.update(dt) else 0

        # If it's not time to spawn yet, just exit
        self.time_acc += dt
        if self.time_acc < self.period_sec:
            return True

        self.time_acc = 0
        self.times += 1
        if self.periods and self.times > self.periods:
            # Won't spawn anymore, but still must wait death of all particles
            self.enabled = live > 0
            return self.enabled

        # It's time to spawn a new batch
        for _ in range(random.randint(self.batch_min, self.batch_max)):
            particle = self.spawn_particle()
            if len(self.emitter) == 2:
                particle.pos = list(self.emitter)
            elif len(self.emitter) == 3:
                particle.pos = list(get_random_point_in_circle(*self.emitter))
            elif len(self.emitter) == 4:
                particle.pos = list(get_random_point_in_rect(*self.emitter))
            if self.color_min and self.color_max:
                particle.color = get_random_color(self.color_min, self.color_max)
            else:
                particle.color = None
            if self.lifetime_min and self.lifetime_max:
                particle.lifetime = get_random_float(self.lifetime_min, self.lifetime_max)
            else:
                particle.lifetime = 0
            if self.velocity_min and self.velocity_max:
                particle.velocity = list(
                    get_random_vector(self.angle_min, self.angle_max, self.velocity_min, self.velocity_max)
                )
            else:
                particle.velocity = [0, 0]
            if self.acceleration_min and self.acceleration_max:
                particle.acceleration = get_random_float(self.acceleration_min, self.acceleration_max)
            else:
                particle.acceleration = 0

    def render(self, surface):
        for particle in self.particles:
            if not particle.enabled:
                continue
            particle.render(surface, self.pos)
