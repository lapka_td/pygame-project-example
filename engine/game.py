from enum import Enum

import pygame

import engine.config
import engine.world
import engine.gui


class Game:
    class ScreenMode(Enum):
        FIXED_WIDTH = 1
        FIXED_HEIGHT = 2
        FIXED_ASPECT = 3

    class State(Enum):
        MENU = 1
        RUN = 2
        EXPLODE = 3
        OVER = 4
        PAUSE = 5

    def __init__(self, game_width: int, game_height: int, game_mode: ScreenMode, resizable: bool = False):
        # Config
        self.window_width = int(engine.config.data.get('Window', 'width', fallback='800'))
        self.window_height = int(engine.config.data.get('Window', 'height', fallback='600'))
        self.vsync = int(engine.config.data.get('Window', 'vsync', fallback='1'))
        if self.vsync != 0 and self.vsync != 1:
            self.vsync = 1
        self.window_mode = engine.config.data.get('Window', 'mode', fallback='windowed')
        if self.window_mode not in {'windowed', 'fullscreen', 'borderless'}:
            self.window_mode = 'windowed'
        self.fps = int(engine.config.data.get('Window', 'fps', fallback='60'))
        if self.fps < 5:
            self.fps = 5
        if self.fps > 500:
            self.fps = 500

        # Desired game mode
        self.game_width = game_width
        self.game_height = game_height
        self.game_mode = game_mode
        self.game_resizable = resizable

        # Create real screen
        if self.window_mode == 'windowed':
            flags = pygame.RESIZABLE if self.game_resizable else 0
            self.screen = pygame.display.set_mode((self.window_width, self.window_height), flags, vsync=self.vsync)
        elif self.window_mode == 'borderless':
            # TODO: check multi-monitor setup
            modes = pygame.display.get_desktop_sizes()
            flags = pygame.NOFRAME
            self.screen = pygame.display.set_mode(modes[0], flags, vsync=self.vsync)
        else:
            flags = pygame.FULLSCREEN
            self.screen = pygame.display.set_mode((self.window_width, self.window_height), flags, vsync=self.vsync)

        # Create fake screen and calculate parameters for scaling
        self.fake_screen = pygame.Surface((self.game_width, self.game_height))
        self.x = self.y = 0
        self.w = self.h = 0
        self.fw = self.fh = 0
        self.screen_area = None
        self.calculate_game_area()

        # Init clock for fps
        self.clock = pygame.time.Clock()
        self.delta = 0

        # Init states
        self.running = True
        self.state = Game.State.MENU
        self.level = 1
        self.current_level_time = 0
        self.time_for_each_level = 5

        self.world = None
        self.gui = None
        self.pressed_keys = None

    def reset(self) -> None:
        self.level = 1
        self.current_level_time = 0
        self.time_for_each_level = 5
        if self.world:
            self.world.reset()

    def calculate_game_area(self) -> None:
        screen_rect = self.screen.get_rect()
        game_aspect = self.game_width / self.game_height
        if self.game_mode == Game.ScreenMode.FIXED_WIDTH:
            self.w = screen_rect.width
            self.h = screen_rect.width / game_aspect
            self.x = 0
            self.y = (screen_rect.height - self.h) / 2
        elif self.game_mode == Game.ScreenMode.FIXED_HEIGHT:
            self.w = screen_rect.height * game_aspect
            self.h = screen_rect.height
            self.x = (screen_rect.width - self.w) / 2
            self.y = 0
        else:
            screen_aspect = screen_rect.width / screen_rect.height
            if game_aspect > screen_aspect:
                self.w = screen_rect.width
                self.h = screen_rect.width / game_aspect
                self.x = 0
                self.y = (screen_rect.height - self.h) / 2
            else:
                self.w = screen_rect.height * game_aspect
                self.h = screen_rect.height
                self.x = (screen_rect.width - self.w) / 2
                self.y = 0
        self.fw = self.game_width / (screen_rect.width - self.x * 2)
        self.fh = self.game_height / (screen_rect.height - self.y * 2)
        self.screen_area = pygame.Rect(
            -self.x * self.fw,
            -self.y * self.fh,
            (screen_rect.width - self.x) * self.fw,
            (screen_rect.height - self.y) * self.fh,
        )

    def calculate_mouse_pos(self, pg_mouse_pos):
        x = (pg_mouse_pos[0] - self.x) * self.fw
        y = (pg_mouse_pos[1] - self.y) * self.fh
        return x, y

    def load(self) -> bool:
        # Create world
        self.world = engine.world.World(self)
        if not self.world.load():
            return False
        # Create GUI
        self.gui = engine.gui.GUI(self)
        if not self.gui.load():
            return False
        return True

    def run(self) -> None:
        while self.running:
            # Get delta time sec
            self.delta = self.clock.tick(self.fps) / 1000

            self.pressed_keys = pygame.key.get_pressed()

            # Process events
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    return
                if event.type == pygame.VIDEORESIZE:
                    self.calculate_game_area()
                    continue
                if not self.gui.consume_event(event):
                    self.world.process_event(event)

            # Update everything
            self.fake_screen.fill((0, 0, 0))
            self.world.update()
            self.gui.update()

            # Draw everything
            self.world.render(self.fake_screen)
            self.gui.render(self.fake_screen)

            # Draw fake screen to real screen
            self.screen.blit(pygame.transform.smoothscale(self.fake_screen, (self.w, self.h)), (self.x, self.y))
            # Swap buffers
            pygame.display.flip()
