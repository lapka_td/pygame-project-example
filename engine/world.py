import random
from enum import Enum

import pygame

import engine.game
from .particles import ParticleSystem
from .utils import blit_rotate, load_image, load_images
from .animation import Animation


class CarCollision(Enum):
    NOTHING = 0
    LEFT = 1
    RIGHT = 2
    FRONT = 3


class World:
    def __init__(self, game):
        self.game = game
        # Balance
        self.player_rot_max = 30
        self.player_rot_speed = 100
        self.player_x_speed = 20
        self.player_speed_y = 200
        self.speed_x_increase_for_level = 1
        self.speed_y_increase_for_level = 20
        # Road dimensions
        self.space_length = 699
        self.road_width = 800
        self.block_size = self.road_width / 4
        self.variants = [
            [['block_1', 0, 1]],
            [['block_2', -200, 2]],
            [['block_2', 600, 2]],
            [['block_2', 0, 2]],
            [['block_1', 0, 1], ['block_2', 600, 2]],
            [['block_2', 400, 2]],
            [['block_3', 0, 3]],
            [['block_2', 0, 2], ['block_1', 600, 1]],
            [['block_1', 0, 1], ['block_2', 400, 2]],
            [['block_3', 200, 3]],
            [['block_1', 200, 1]],
            [['block_2', 200, 2]],
            [['block_1', 400, 1]],
            [['block_1', 200, 1], ['block_1', 600, 1]],
            [['block_1', 0, 1], ['block_1', 400, 1]],
            [['block_2', -200, 2], ['block_1', 400, 1]],
        ]
        self.left = self.game.game_width / 2 - self.road_width / 2
        self.blocks_offset = 0
        self.player_rot = 0
        self.crash_time = 0
        self.blocks = None
        self.player_x = self.game.game_width / 2
        self.player_y = 0
        self.player_half_w = self.player_half_h = 0
        self.assets = None
        self.level_up_sound = None
        self.collision_channel = None
        self.collision_sound = None
        self.hit_sound = None
        self.motor_sound = None
        self.motor_channel = None
        self.player_rect = None
        self.explosion = None
        self.smoke_animation = None
        self.smoke = None
        self.sparkles = None

    def reset(self) -> None:
        self.blocks_offset = 0
        self.player_rot = 0
        self.crash_time = 0
        self.blocks = None
        self.rebuild_blocks()
        self.player_x = self.game.game_width / 2

    def rebuild_blocks(self) -> None:
        if not self.blocks:
            self.blocks = [[]]
            for _ in range(2):
                self.blocks.append(self.variants[random.randint(0, len(self.variants) - 1)])
            return
        y_speed = (self.player_speed_y + self.speed_y_increase_for_level * (self.game.level - 1))
        self.blocks_offset += self.game.delta * y_speed
        if self.blocks_offset > self.space_length:
            self.blocks_offset -= self.space_length
            del self.blocks[0]
            self.blocks.append(self.variants[random.randint(0, len(self.variants) - 1)])

    def load(self) -> bool:
        self.assets = {
            'road': load_image('road.png', alpha=False),
            'car': load_image('car.png'),
            'car_dead': load_image('car_dead.png'),
            'block_1': load_image('block_1.png'),
            'block_2': load_image('block_2.png'),
            'block_3': load_image('block_3.png'),
        }
        self.level_up_sound = pygame.mixer.Sound("assets/sounds/level.wav")
        self.level_up_sound.set_volume(0.5)
        self.collision_channel = pygame.mixer.Channel(2)
        self.collision_sound = pygame.mixer.Sound("assets/sounds/collide.ogg")
        self.collision_sound.set_volume(0.5)
        self.hit_sound = pygame.mixer.Sound("assets/sounds/hit.ogg")
        self.motor_sound = pygame.mixer.Sound("assets/sounds/motor.ogg")
        self.motor_channel = pygame.mixer.Channel(3)
        self.player_rect = self.assets['car'].get_rect()
        self.player_x = self.game.game_width / 2
        self.player_y = self.game.game_height - self.player_rect.h
        self.player_half_w = self.player_rect.w / 2
        self.player_half_h = self.player_rect.h / 2
        self.explosion = Animation(images=load_images('explosion'), img_dur=0.1, loop=False)
        self.explosion.done = True
        self.smoke_animation = Animation(images=load_images('smoke'), img_dur=0.05, loop=False)
        self.smoke = ParticleSystem(
            (0, 0),
            (0, 0),
            animation=self.smoke_animation,
            period_sec=0.01,
            batch_min=1,
            batch_max=3,
            velocity_min=10,
            velocity_max=100,
        )
        self.sparkles = ParticleSystem(
            (0, 0),
            (0, 0),
            period_sec=0.01,
            periods=1,
            batch_min=1,
            batch_max=5,
            color_min=(200, 0, 0),
            color_max=(255, 200, 0),
            velocity_min=10,
            velocity_max=100,
            lifetime_min=0.01,
            lifetime_max=0.2,
        )
        return True

    def process_event(self, event) -> None:
        pass

    def update_collision_sound(self, collision: CarCollision) -> None:
        is_playing = self.collision_channel.get_busy()
        if collision == CarCollision.NOTHING:
            if is_playing:
                self.collision_channel.stop()
        else:
            if not is_playing:
                self.collision_channel.play(self.collision_sound)

    def update_motor_sound(self) -> None:
        is_playing = self.motor_channel.get_busy()
        state = engine.game.Game.State
        if self.game.state != state.RUN:
            if is_playing:
                self.motor_channel.stop()
        else:
            if not is_playing:
                self.motor_channel.play(self.motor_sound, loops=-1)

    def collide_car(self) -> CarCollision:
        player_rect = pygame.Rect(
            self.player_x - self.player_half_w,
            self.player_y - self.player_half_h,
            self.player_rect.w,
            self.player_rect.h
        )
        # Front collide
        rect_for_front_collide = player_rect.inflate(-20, 0)
        for i, row in enumerate(self.blocks):
            y = self.game.game_height - i * self.space_length - self.block_size + self.blocks_offset
            if y > self.player_y - self.player_half_h:
                continue
            for block in row:
                x = self.left + block[1]
                block_w = self.block_size * block[2]
                if rect_for_front_collide.colliderect(pygame.Rect(x, y, block_w, self.block_size)):
                    return CarCollision.FRONT
        # Side collide
        for i, row in enumerate(self.blocks):
            y = self.game.game_height - i * self.space_length - self.block_size + self.blocks_offset
            for block in row:
                x = self.left + block[1]
                block_w = self.block_size * block[2]
                if player_rect.colliderect(pygame.Rect(x - 1, y, block_w + 2, self.block_size)):
                    if self.player_x > x:
                        self.player_x = x + self.block_size * block[2] + self.player_half_w
                        return CarCollision.LEFT
                    else:
                        self.player_x = x - self.player_half_w
                        return CarCollision.RIGHT

        # Roadside
        if self.player_x <= self.left + self.player_half_w:
            self.player_x = self.left + self.player_half_w
            return CarCollision.LEFT
        if self.player_x >= self.left + self.road_width - self.player_half_w:
            self.player_x = self.left + self.road_width - self.player_half_w
            return CarCollision.RIGHT

        return CarCollision.NOTHING

    def update_level(self) -> None:
        self.game.current_level_time += self.game.delta
        if self.game.current_level_time > self.game.time_for_each_level:
            self.game.level += 1
            self.game.current_level_time = 0
            self.level_up_sound.play()

    def update_particles_pos(self, collision: CarCollision) -> None:
        dt = self.game.delta
        y_speed = (self.player_speed_y + self.speed_y_increase_for_level * (self.game.level - 1))
        self.sparkles.enabled = collision != CarCollision.NOTHING
        if collision == CarCollision.NOTHING:
            self.sparkles.periods = 1
        elif collision == CarCollision.LEFT:
            self.sparkles.periods = 0
            self.sparkles.emitter = (
                self.player_x - self.player_half_w + 10,
                self.player_y - self.sparkles.pos[1] + self.player_half_h,
                10,
            )
        elif collision == CarCollision.RIGHT:
            self.sparkles.periods = 0
            self.sparkles.emitter = (
                self.player_x + self.player_half_w - 10,
                self.player_y - self.sparkles.pos[1] + self.player_half_h,
                10,
            )
        self.sparkles.pos[1] += dt * y_speed

        self.smoke.emitter = (self.player_x, self.player_y - self.smoke.pos[1] + self.player_half_h, 20)
        self.smoke.pos[1] += dt * y_speed

    def update(self) -> None:
        dt = self.game.delta
        self.smoke.update(dt)
        self.update_motor_sound()
        state = engine.game.Game.State
        if self.game.state == state.RUN:
            self.update_level()
            self.sparkles.update(dt)
            self.rebuild_blocks()
            pressed_keys = self.game.pressed_keys
            left_pressed = pressed_keys[pygame.K_LEFT]
            right_pressed = pressed_keys[pygame.K_RIGHT]

            collision = self.collide_car()
            self.update_collision_sound(collision)

            if collision == CarCollision.FRONT:
                self.game.state = state.EXPLODE
                left_pressed = False
                right_pressed = False
                self.player_rot = 0
                self.explosion.reset()
                self.hit_sound.play()
            elif collision == CarCollision.LEFT:
                left_pressed = False
            elif collision == CarCollision.RIGHT:
                right_pressed = False

            if left_pressed:
                self.player_rot -= dt * self.player_rot_speed
                if self.player_rot < -self.player_rot_max:
                    self.player_rot = -self.player_rot_max
            if right_pressed:
                self.player_rot += dt * self.player_rot_speed
                if self.player_rot > self.player_rot_max:
                    self.player_rot = self.player_rot_max
            if not left_pressed and not right_pressed:
                if self.player_rot > 0:
                    self.player_rot -= dt * self.player_rot_speed
                if self.player_rot < 0:
                    self.player_rot += dt * self.player_rot_speed
                if abs(self.player_rot) < 2:
                    self.player_rot = 0

            x_speed = (self.player_x_speed + self.speed_x_increase_for_level * (self.game.level - 1))
            self.player_x += dt * self.player_rot * x_speed
            self.update_particles_pos(collision)

        if self.game.state == state.EXPLODE:
            self.update_collision_sound(CarCollision.NOTHING)
            self.explosion.update(dt)
            if self.explosion.done:
                self.game.state = state.OVER

    def render(self, surface) -> None:
        state = engine.game.Game.State
        if self.game.state in {state.MENU, state.OVER, state.PAUSE}:
            return

        for i in range(len(self.blocks)):
            y = self.game.game_height - i * self.space_length - self.block_size * 2 + self.blocks_offset
            if y < -self.space_length:
                continue
            surface.blit(self.assets['road'], (0, y))

        self.smoke.render(surface)
        if self.game.state == state.EXPLODE:
            blit_rotate(
                surface,
                self.assets['car_dead'],
                (self.player_x, self.player_y),
                (self.player_half_w, self.player_half_h),
                0,
            )
        else:
            self.sparkles.render(surface)
            blit_rotate(
                surface,
                self.assets['car'],
                (self.player_x, self.player_y),
                (self.player_half_w, self.player_half_h),
                -self.player_rot,
            )

        for i, row in enumerate(self.blocks):
            y = self.game.game_height - i * self.space_length - self.block_size + self.blocks_offset
            for block in row:
                x = self.left + block[1]
                surface.blit(self.assets[block[0]], (x, y))

        if self.game.state == state.EXPLODE:
            explosion = self.explosion.img()
            rect = explosion.get_rect()
            surface.blit(explosion, (self.player_x - rect.w / 2, self.player_y - rect.h / 2))
