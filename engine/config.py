import configparser

config_path = 'assets/config.cfg'
sections = ['Window']

data = configparser.ConfigParser()
data.read(config_path)
for section in sections:
	if not data.has_section(section):
		data.add_section(section)


def save() -> None:
	global data
	global config_path
	with open(config_path, 'w') as configfile:
		data.write(configfile)
