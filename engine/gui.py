import pygame

import engine.game
from .utils import load_image


class GUI:
    def __init__(self, game):
        self.game = game
        self.focused_key = 0
        self.background_speed = -50
        self.background_y = 0
        self.menu_font = None
        self.font = None
        self.assets = None

    def load(self) -> bool:
        self.menu_font = pygame.font.SysFont("Arial", 200)
        self.font = pygame.font.SysFont("Arial", 30)
        self.assets = {
            'arrow': load_image('arrow.png'),
            'menu': load_image('menu.png', alpha=False),
            'pause': load_image('pause.png', alpha=False),
            'over': load_image('over.png', alpha=False),
        }
        return True

    def update(self) -> None:
        state = engine.game.Game.State
        if self.game.state != state.MENU:
            return
        self.background_y += self.background_speed * self.game.delta
        if self.background_speed < 0 and self.background_y <= -self.game.game_height:
            self.background_speed = -self.background_speed
        if self.background_speed > 0 and self.background_y >= 0:
            self.background_speed = -self.background_speed

    def consume_event(self, event) -> bool:
        state = engine.game.Game.State
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_RETURN:
                if self.game.state == state.MENU or self.game.state == state.PAUSE:
                    if self.focused_key == 0:
                        self.game.state = state.RUN
                    else:
                        self.game.running = False
                elif self.game.state == state.OVER:
                    if self.focused_key == 0:
                        self.game.reset()
                        self.game.state = state.RUN
                    else:
                        self.game.running = False
                return True
            if event.key == pygame.K_ESCAPE:
                if self.game.state == state.MENU or self.game.state == state.OVER:
                    self.game.running = False
                elif self.game.state == state.PAUSE:
                    self.game.state = state.RUN
                elif self.game.state == state.RUN:
                    self.game.state = state.PAUSE
                return True
            if event.key == pygame.K_UP:
                self.focused_key = 0
                return True
            if event.key == pygame.K_DOWN:
                self.focused_key = 1
                return True
        return False

    def draw_button(self, surface, text: str, pos: int) -> None:
        text = self.menu_font.render(text, 1, pygame.Color(0, 0, 0))
        x_pos = self.game.game_width / 2 - text.get_width() / 2
        y_pos = self.game.game_height * 0.25 * (pos + 1)
        surface.blit(text, (x_pos + 10, y_pos + 10))
        if self.focused_key == pos:
            surface.blit(self.assets['arrow'], (x_pos - 200, y_pos))

    def draw_level(self, surface, back: bool):
        text = self.font.render('Level: ' + str(self.game.level), 1, pygame.Color(0, 255, 0))
        text_w = text.get_width()
        border = 25
        x_pos = self.game.game_width / 2 - text_w / 2
        y_pos = 10
        if back:
            pygame.draw.rect(
                surface,
                (0, 0, 0),
                (x_pos - border, y_pos - border, text_w + border * 2, text.get_height() + border * 2),
            )
        surface.blit(text, (x_pos, y_pos))

    def render(self, surface) -> None:
        state = engine.game.Game.State
        if self.game.state == state.RUN or self.game.state == state.EXPLODE:
            self.draw_level(surface, False)
        elif self.game.state == state.MENU:
            surface.blit(self.assets['menu'], (0, self.background_y))
            self.draw_button(surface, 'Start', 0)
            self.draw_button(surface, 'Exit', 1)
        elif self.game.state == state.OVER:
            surface.blit(self.assets['over'], (0, 0))
            self.draw_level(surface, True)
            self.draw_button(surface, 'Restart', 0)
            self.draw_button(surface, 'Exit', 1)
        elif self.game.state == state.PAUSE:
            surface.blit(self.assets['pause'], (0, 0))
            self.draw_button(surface, 'Continue', 0)
            self.draw_button(surface, 'Exit', 1)
        # FPS counter
        text = self.font.render(str(int(self.game.clock.get_fps())), 1, pygame.Color(255, 0, 0))
        x_pos = self.game.screen_area.x if self.game.screen_area.x > 0 else 0
        y_pos = self.game.screen_area.y if self.game.screen_area.y > 0 else 0
        surface.blit(text, (x_pos + 10, y_pos + 10))
