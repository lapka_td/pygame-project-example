class Animation:
    def __init__(self, images: list, img_dur=1, loop=True):
        self.images = images
        self.last = len(self.images) - 1
        self.loop = loop
        self.img_duration = img_dur
        self.frame = 0
        self.done = False
        self.time_acc = 0

    def reset(self):
        self.frame = 0
        self.done = False
        self.time_acc = 0

    def copy(self):
        return Animation(self.images, self.img_duration, self.loop)

    def update(self, dt: float):
        if self.done:
            return
        self.time_acc += dt
        while self.time_acc >= self.img_duration:
            self.time_acc -= self.img_duration
            self.frame += 1
        if self.frame > self.last:
            if self.loop:
                self.frame = self.frame % self.last - 1
            else:
                self.frame = self.last
                self.done = True

    def img(self):
        return self.images[self.frame]
