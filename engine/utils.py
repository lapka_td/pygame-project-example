import os
import random
import math

import pygame

BASE_IMG_PATH = 'assets/images/'


def load_image(path: str, alpha=True):
    if alpha:
        img = pygame.image.load(BASE_IMG_PATH + path)
    else:
        img = pygame.image.load(BASE_IMG_PATH + path).convert()
        img.set_colorkey((0, 0, 0))
    return img


def load_images(path, alpha=True):
    images = []
    for img_name in sorted(os.listdir(BASE_IMG_PATH + path)):
        images.append(load_image(path + '/' + img_name, alpha))
    return images


def blit_rotate(surf, image, pos, origin_pos, angle):
    # offset from pivot to center
    image_rect = image.get_rect(topleft=(pos[0] - origin_pos[0], pos[1] - origin_pos[1]))
    offset_center_to_pivot = pygame.math.Vector2(pos) - image_rect.center
    # rotated offset from pivot to center
    rotated_offset = offset_center_to_pivot.rotate(-angle)
    # rotated image center
    rotated_image_center = (pos[0] - rotated_offset.x, pos[1] - rotated_offset.y)
    # get a rotated image
    rotated_image = pygame.transform.rotate(image, angle)
    rotated_image_rect = rotated_image.get_rect(center=rotated_image_center)
    # rotate and blit the image
    surf.blit(rotated_image, rotated_image_rect)
    # draw rectangle around the image
    # pygame.draw.rect(surf, (255, 0, 0), (*rotated_image_rect.topleft, *rotated_image.get_size()), 2)


def get_random_point_in_circle(center_x, center_y, radius):
    a = 2 * math.pi * random.random()
    r = radius * math.sqrt(random.random())
    x = r * math.cos(a) + center_x
    y = r * math.sin(a) + center_y
    return x, y


def get_random_point_in_rect(x, y, w, h):
    px = x + w * random.random()
    py = y + h * random.random()
    return px, py


def get_random_color(color_min, color_max):
    r = random.randint(color_min[0], color_max[0])
    g = random.randint(color_min[1], color_max[1])
    b = random.randint(color_min[2], color_max[2])
    return r, g, b


def get_random_float(min_value, max_value) -> float:
    if min_value == max_value:
        return min_value
    return min_value + (max_value - min_value) * random.random()


def get_random_vector(angle_min, angle_max, length_min, length_max):
    a_min = math.radians(angle_min)
    a_max = math.radians(angle_max)
    a = a_min + (a_max - a_min) * random.random()
    r = length_min + (length_max - length_min) * random.random()
    x = r * math.cos(a)
    y = r * math.sin(a)
    return x, y
