### Requirements
Python >= 3.7

### Install
```
python -m venv venv
pip install --upgrade pip
pip install -r requirements.txt
```

### Run dev
```
.\venv\Scripts\activate
python .\main.py
```

### Build
```
.\venv\Scripts\activate
python -m PyInstaller main.py --windowed --hidden-import pygame --distpath ./ --icon=appicon.ico --name=game
```
And you can distribute `game` folder (place `assets` into it)
