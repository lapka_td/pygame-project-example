import pygame

from engine import Game


# Init PyGame
pygame.mixer.init()
pygame.init()

# Create game engine
game_width = 2000
game_height = 1000
game_mode = Game.ScreenMode.FIXED_HEIGHT
game_resizable = True
game = Game(game_width, game_height, game_mode, game_resizable)

# Init window title and icon
pygame.display.set_caption('Racing')
pygame.display.set_icon(pygame.image.load('assets/icon.png'))

# Create engine components, load all assets and start game loop
if game.load():
    game.run()

# Cleaning
pygame.mixer.music.stop()
pygame.mixer.quit()
pygame.quit()
